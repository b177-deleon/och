const Products = require("../models/Products");

// Create Product (Admin only)
module.exports.addProduct = (items) => {
	if (items.isAdmin) {
		let newProduct = new Products({
			name : items.product.name,
			description : items.product.description,
			price : items.product.price
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} 
			else {
				return true;
			};
		});
	}
	else {
		return Promise.resolve(false);
	};
};

// [EDITED by YHAJE]
// Retrieve All Active Products
module.exports.getActiveProducts = () => {
	return Products.find({isActive : true}).then(result => {
		return result;
	})
}

// Retrieve All Products
module.exports.getAllProducts = (data) => {
	if(data.isAdmin) {
		return Products.find({}).then(result => {
			return result;
		})
	}
}
// END

// Retrieve Single Product
module.exports.getProduct = (reqParams) => {
	return Products.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update Product information (Admin only)
module.exports.updateProduct = (data) => {
	if(data.isAdmin) {
		let updatedProduct = {
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		}
		return Products.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else {
		return Promise.resolve("Access denied.");
	}
}

// Archive Product (Admin only)
module.exports.archiveProduct = (archive) => {
	if (archive.isAdmin) {
		let updateActive = {
			isActive : false
		};
		return Products.findByIdAndUpdate(archive.productId, updateActive).then((product, error) => {
			if (error) {
				return false;
			} 
			else {
				return true;
			}
		});
	} 
	else {
		return Promise.resolve(`Can not proceed as user in not an Admin.`);
	}
};

// Added by Yhaje
// Activate
module.exports.activateProduct = (activate) => {
	if (activate.isAdmin) {
		let updateActive = {
			isActive : true
		};
		return Products.findByIdAndUpdate(activate.productId, updateActive).then((product, error) => {
			if (error) {
				return false;
			} 
			else {
				return true;
			}
		});
	} 
	else {
		return Promise.resolve(`Can not proceed as user in not an Admin.`);
	}
};
// End