import React, { useEffect, useState, Fragment } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import { Grid, Input, InputAdornment } from '@mui/material';
import SearchTwoToneIcon from '@mui/icons-material/SearchTwoTone';

export default function Products() {

	const [search, setSearch] = useState('');

	const [products, setProduct] = useState([]);

	useEffect(() => {
		fetch('http://localhost:4000/products/active')
			.then((res) => res.json())
			.then((data) => {
				setProduct(data);
			});
	}, []);

	return (
		<Fragment>
			<Container
				style={{
					display: 'flex',
					justifyContent: 'center',
					marginTop: '40px',
					backgroundColor: 'gray',
				}}
			>
				<Input
					style={{
						height: '50%',
						width: '50%',
						marginTop: '10px',
						marginBottom: '50px',
						color: 'white',
					}}
					type="text"
					placeholder="Looking for something?"
					onChange={(e) => {
						setSearch(e.target.value);
					}}
					startAdornment={
						<InputAdornment position="start">
							<SearchTwoToneIcon />
						</InputAdornment>
					}
				/>
			</Container>

			<Grid
				container
				direction="row"
				justifyContent="space-evenly"
				alignItems="center"
				columns={{ md: 6 }}
				style={{ margin: '5px' }}
				item
				spacing={3}
			>
				{products
					.filter((product) => {
						if (search === '') {
							return product;
						} else if (
							product.name.toLowerCase().includes(search.toLowerCase())
						) {
							return product;
						}
					})
					.map((product) => {
						return (
							<Grid md={2}>
								<ProductCard key={product._id} productProp={product} />
							</Grid>
						);
					})}
			</Grid>
		</Fragment>
	);
}
